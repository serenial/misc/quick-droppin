# Quick Droppin' - The Quick Drop Training Game

Quick Drop can really improve your programming speed but it can be tricky trying to hold all the different Ctrl-Key and Object Shortcuts in mind.

I made this game over the 2020 winter holidays as a way of building my skills and as a bit of fun.

![animation showing game being played](https://gitlab.com/serenial/misc/quick-droppin/-/raw/main/img/run-through.gif)

## Dependencies
LabVIEW 2020 for development but a 2014 version is available in the releases for usage.

## Usage
The code runs in the development environment.

Clone this repo / download the source and run `Main.vi`.

You have the option to select what type of challenges you wish to be tested on
* _"Ctrl-Key shortcuts"_ are those quick drop shortcuts like "insert into wire" or "remove and rewire"
* _"Object Shortcuts"_ are the abbreviated names for various nodes/structures/constants like "nc" for numeric constant

- Note: Only Block Diagram shortcuts are currently implemented.

The game duration can be selected (30 to 60 seconds seems to be a good length) and you can set the option for passing preference (i.e. the option to skip a challenge).

Once ready - hit the _"Start Game"_ button and get ready!

You will be presented with one challenge at a time. Once you have placed the specified item or completed the specified action, the challenge will scored and a new challenge opened.

The "game bar" gives you the options to
* _Pass_ (if enabled)
* _Show Hints_ - which will show you the default shortcut key combination
* _Reset_ the current challenge if you make a mess of it
* _Exit_


## Tips
*Super Quick Drop* - if you click your mouse with quick drop open then the top item in the list will be dropped where your mouse clicked.


## Operation
The challenges are based on a parent `Challenge` class and are loaded from disk when the game starts. Most of the functionality of the class can be overridden but the basic parent class provides functionality to compare a clone of a `Template.vi` to a `Solution.vi` by traversing the block diagram for nodes, their interconnections and constants. This is not the most efficient test and is somewhat fragile but seems to work in most cases and avoids custom implementation for each of the 100+ challenges.

Custom challenges can be added by you and there are some poorly written and documented tools in the project's tools folder that were used to aid in challenge creation.

## Issues/PRs
Both very welcome but I might not be able to give this light-hearted project my full attention.
